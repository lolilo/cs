package upprpo.cs.roles;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private RolesEnum name;

    public Role() {}
    public Role(RolesEnum role) {this.name = role;}

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(RolesEnum name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public RolesEnum getName() {
        return name;
    }
}
