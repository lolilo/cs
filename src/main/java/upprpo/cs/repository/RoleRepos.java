package upprpo.cs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import upprpo.cs.roles.Role;
import upprpo.cs.roles.RolesEnum;

import java.util.Optional;

public interface RoleRepos extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(RolesEnum name);
}
