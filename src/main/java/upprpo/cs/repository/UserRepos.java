package upprpo.cs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import upprpo.cs.roles.User;

import java.util.Optional;

public interface UserRepos extends JpaRepository<User, Integer> {
    Optional<User> findUserByLogin(String login);
    Boolean existsByLogin(String login);
}
