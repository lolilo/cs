package upprpo.cs.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import upprpo.cs.repository.UserRepos;
import upprpo.cs.roles.User;

@Service
public class UserDetailsServiceImpl  implements UserDetailsService {
    @Autowired
    UserRepos userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findUserByLogin(s).orElseThrow(() -> new UsernameNotFoundException(s + " not found;"));
        return UserDetailsImpl.build(user);
    }
}
